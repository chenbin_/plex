﻿from ast import *
from plex_lexer import *
from plex_parser import *

import functools

id = Tag(ID)
num = Tag(INT) ^ (lambda i: int(i))


def keyword(kw):
    return Reserved(kw, RESERVED)


def aexp_value():
    return (num ^ (lambda i: IntAexp(i))) | (id ^ (lambda v: VarAexp(v)))


def process_group(parsed):
    ((_, p), _) = parsed
    return p


def aexp():
    pass


def aexp_group():
    return keyword('(') + Lazy(aexp) + keyword(')') ^ process_group


def aexp_term():
    return aexp_value() | aexp_group()


if __name__ == '__main__':
    ops = [
        ['*', '/'],
        ['+', '-'],
    ]

    s = [keyword(op) for op in ops]
    p = functools.reduce(lambda l, r: l|r, s)
    for ss in s:
        print(ss.value)
    print(p.left, p.right)

