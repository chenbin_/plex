import sys
from impl_parser import *

if __name__ == '__main__':
    fn = sys.argv[1]
    fd = open(fn, 'r')
    characters = fd.read()
    fd.close()
    tokens = imp_lex(characters)