﻿import sys
import re

RESERVED = 'RESERVED'
INT = 'INT'
ID = 'ID'

token_exprs = [
    (r'[ \n\t]+', None),
    (r'#[^\n]*', None),
    (r':=', RESERVED),
    (r'\(', RESERVED),
    (r'\)', RESERVED),
    (r';', RESERVED),
    (r'\+', RESERVED),
    (r'-', RESERVED),
    (r'\*', RESERVED),
    (r'/', RESERVED),
    (r'<=', RESERVED),
    (r'<', RESERVED),
    (r'>=', RESERVED),
    (r'>', RESERVED),
    (r'=', RESERVED),
    (r'!=', RESERVED),
    (r'and', RESERVED),
    (r'or', RESERVED),
    (r'not', RESERVED),
    (r'if', RESERVED),
    (r'then', RESERVED),
    (r'else', RESERVED),
    (r'while', RESERVED),
    (r'do', RESERVED),
    (r'end', RESERVED),
    (r'[0-9]+', INT),
    (r'[A-Za-z][A-Za-z0-9_]*', ID),
]


def lex(chars, exprs):
    pos = 0
    tokens = []
    while pos < len(chars):
        match = None
        for token_expr in exprs:
            pattern, tag = token_expr
            regex = re.compile(pattern)
            match = regex.match(chars, pos)
            if match:
                text = match.group(0)
                if tag:
                    token = (text, tag)
                    tokens.append(token)
                break
        if not match:
            sys.stderr.write('Illegal charater: %s' % chars[pos])
            sys.exit(1)
        else:
            pos = match.end(0)
    return tokens


def imp_lex(chars):
    return lex(chars, token_exprs)

if __name__ == "__main__":
    fn = sys.argv[1]
    with open(fn, 'r') as fd:
        characters = fd.read()
        tokens = imp_lex(characters)
        for token in tokens:
            print(token)
